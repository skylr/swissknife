function Renderer(assets)
{

    var scene = new THREE.Scene();

    var camera = new THREE.PerspectiveCamera(5, window.innerWidth / window.innerHeight, 10, 1000);

    var renderer = new THREE.WebGLRenderer({ antialias : true});
    renderer.setClearColor(new THREE.Color(0xeeeeee));
    renderer.setSize(window.innerWidth, window.innerHeight);

    // Watch window and resize
    window.addEventListener( 'resize', onWindowResize, false );
    function onWindowResize(){

        camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();

        renderer.setSize( window.innerWidth, window.innerHeight );

    }


    var gl = renderer.context;

    camera.position.x = 0;
    camera.position.y = 500;
    camera.position.z = 0;
    camera.lookAt(scene.position);




//LIGHTS

	// key
    var dirlight = new THREE.DirectionalLight(0xcbcdcc);
    dirlight.intensity = 0.4;
    dirlight.position.set(.3, 1, 0);
    dirlight.target.position.set(0, 0, 0);
    scene.add(dirlight);

//     fill
    var dirlight2 = new THREE.DirectionalLight(0xcbcdcc);
    dirlight2.intensity = 0.4;
    dirlight2.position.set(-1, 0, -1);
    dirlight2.target.position.set(0, 0, 0);
    scene.add(dirlight2);

    //back
    var dirlight3 = new THREE.DirectionalLight(0xcbcdcc);
    dirlight3.intensity = 0.4;
    dirlight3.position.set(-1, 0.5, 0);
    dirlight3.target.position.set(0, 0, 0);
    scene.add(dirlight3);

    // screen left
    var dirlight4 = new THREE.DirectionalLight(0xcbcdcc);
    dirlight4.intensity = 0.4;
    dirlight4.position.set(0, 0, 1);
    dirlight4.target.position.set(0, 0, 0);
    scene.add(dirlight4);

//     front
    var dirlight5 = new THREE.DirectionalLight(0xcbcdcc);
    dirlight5.intensity = 0.4;
    dirlight5.position.set(1, 1, 1);
    dirlight5.target.position.set(0, 0, 0);
    scene.add(dirlight5);

//         screen left
    var dirlight6 = new THREE.DirectionalLight(0xcbcdcc);
    dirlight6.intensity = 0.4;
    dirlight6.position.set(0, 0, -1);
    dirlight6.target.position.set(0, 0, 0);
    scene.add(dirlight6);

    var amblight = new THREE.AmbientLight(0xeff0ef);
    amblight.intensity = 0.4;
    scene.add(amblight);

	    //Load env map
    var envTexture = new THREE.Texture();
    envTexture.mapping = THREE.SphericalReflectionMapping;
    envTexture.image = assets.textures.envMap;
	envTexture.needsUpdate = true;


	var sphereGeo = new THREE.SphereGeometry(1000, 32, 32);
 	var	sphereMat = new THREE.MeshBasicMaterial({
 		side: THREE.BackSide,
 		map: envTexture,
 	});

 	sphereMesh = new THREE.Mesh(sphereGeo, sphereMat);
 	scene.add(sphereMesh);



	cameraPosition = new THREE.Vector3();

    var orbitControls = new THREE.OrbitControls(camera, document.getElementById('WebGL-output'));


    var clock = new THREE.Clock();

    document.getElementById("WebGL-output").appendChild(renderer.domElement);



    //podium_21oz bottle


    var handelTexture = new THREE.Texture();
	handelTexture.image = assets.textures.color_map;
	handelTexture.needsUpdate = true;

	handelTexture.anisotropy = 5;



	var metalmtl =  new THREE.MeshStandardMaterial({
    	side : THREE.DoubleSide,
    	color : 0xffffff,
    	roughness : 0.6,
    	metalness : 0.9,
    	envMap : envTexture,
    	envMapIntensity : 0.5,
    	//map : metalBodyTexture,
    	//roughnessMap : metalRoughnessTexture,
    	//metalnessMap : metalBodymetalnessTexture,
    	//envMap : assets.cubemaps.cubemap,
    });

    var handlemtl =  new THREE.MeshStandardMaterial({
    	side : THREE.Double,
    	map: handelTexture,
    	color : 0xffffff,
    	roughness : 0.35,
    	metalness : 0.05,
    	envMap : envTexture,
    	envMapIntensity : 0.7,
    	//map : metalBodyTexture,
    	//roughnessMap : metalRoughnessTexture,
    	//metalnessMap : metalBodymetalnessTexture,
    	//envMap : assets.cubemaps.cubemap,
    });

    var tweezmtl =  new THREE.MeshStandardMaterial({
    	side : THREE.Double,
    	color : 0xcccccc,
    	roughness : 0.3,
    	metalness : 0.1,
    	envMap : envTexture,
    	envMapIntensity : 0.7,
    	//map : metalBodyTexture,
    	//roughnessMap : metalRoughnessTexture,
    	//metalnessMap : metalBodymetalnessTexture,
    	//envMap : assets.cubemaps.cubemap,
    });

    var pickmtl =  new THREE.MeshStandardMaterial({
    	side : THREE.Double,
    	color : 0xcccccc,
    	roughness : 0.3,
    	metalness : 0.1,
    	envMap : envTexture,
    	envMapIntensity : 0.5,
    	//map : metalBodyTexture,
    	//roughnessMap : metalRoughnessTexture,
    	//metalnessMap : metalBodymetalnessTexture,
    	//envMap : assets.cubemaps.cubemap,
    });

    function loadMovableKnifeComponent(name) {
      assets.models[name].children.forEach(function(child) {
        child.material = metalmtl;
      });

      // create a pivot point container obj and add pivot point mesh to it
      knife[name].pivotPointObject = new THREE.Object3D();
      knife[name].pivotPointObject.position.copy(knife[name].pivotPointLocation);
      scene.add(knife[name].pivotPointObject);

      // add this component to its pivot point
      knife[name].pivotPointObject.add(assets.models[name]);

      // set offset of this model relative to its pivot point
      assets.models[name].position.copy(knife[name].componentOffset);
    }

    for (var componentName in knife) {
        loadMovableKnifeComponent(componentName);
    }


    // keep all spacers as-is
    assets.models.spacer_0.children.forEach(function(child)
    {
        child.material = metalmtl;
    });
    scene.add(assets.models.spacer_0);

    assets.models.spacer_1.children.forEach(function(child)
    {
        child.material = metalmtl;
    });
    scene.add(assets.models.spacer_1);

    assets.models.spacer_2.children.forEach(function(child)
    {
        child.material = metalmtl;
    });
    scene.add(assets.models.spacer_2);

    assets.models.thin_spacer.children.forEach(function(child)
    {
        child.material = metalmtl;
    });
    scene.add(assets.models.thin_spacer);

    assets.models.thin_spacer_2.children.forEach(function(child)
    {
        child.material = metalmtl;
    });
    scene.add(assets.models.thin_spacer_2);

    assets.models.thin_spacer_3.children.forEach(function(child)
    {
        child.material = metalmtl;
    });
    scene.add(assets.models.thin_spacer_3);

    assets.models.thin_spacer_4.children.forEach(function(child)
    {
        child.material = metalmtl;
    });
    scene.add(assets.models.thin_spacer_4);

    assets.models.pick.children.forEach(function(child)
    {
        child.material = metalmtl;
    });
    scene.add(assets.models.pick);

	assets.models.top.children.forEach(function(child)
    {
        child.material = handlemtl;
    });
    scene.add(assets.models.top);

    assets.models.bottom.children.forEach(function(child)
    {
        child.material = handlemtl;
    });
    scene.add(assets.models.bottom);

    assets.models.tweezers.children.forEach(function(child)
    {
        child.material = tweezmtl;
    });
    scene.add(assets.models.tweezers);






    function tweenToSplayedPositions(duration) {
      for (var componentName in knife) {
        // calculate the desired rotation in radians from splayed param
        var rotationToY = knife[componentName].rotationRadians.splayed * Math.PI;
        var toEuler = new THREE.Euler(0, rotationToY, 0, 'XYZ');
        var fromEuler = knife[componentName].pivotPointObject.rotation.clone();

        makeRotateTween(knife[componentName].pivotPointObject, toEuler, fromEuler, duration);
      }
    }

    function tweenToOriginalPositions(duration) {
        for (var componentName in knife) {
            var rotationToY = knife[componentName].rotationRadians.closed;
            var toEuler = new THREE.Euler(0, rotationToY, 0, 'XYZ');
            var fromEuler = knife[componentName].pivotPointObject.rotation.clone();

            makeRotateTween(knife[componentName].pivotPointObject, toEuler, fromEuler, duration);
        }
    }

    function makeRotateTween(obj, toEuler, fromEuler, duration) {
        var tween = new TWEEN.Tween(fromEuler)
          .to(toEuler, duration)
          .onUpdate(function() {
              obj.rotation.y = fromEuler.y;
          })
          .start();
    }


    function explodeXAxis(duration) {
      var count = 0;
      for (var componentName in knife) {
        var fromPosition = knife[componentName].pivotPointObject.position.clone();
        var newXPos, toPosition;

        // first 4 objects
        if (count < 4) {
          newXPos = fromPosition.x + (count * 7) + 8;
          toPosition = new THREE.Vector3(newXPos, 0, 0);
        } else {
          // next 4 objects
          newXPos = fromPosition.x - ((count-4) * 8) - 8;
          toPosition = new THREE.Vector3(newXPos, 0, 0);
        }

        var explodeTween = new TWEEN.Tween(knife[componentName].pivotPointObject.position)
          .to(toPosition, duration)
          .start();

        count++;
      }

    }

    function implodeXAxis(duration) {
        for (var componentName in knife) {
            var fromPosition = knife[componentName].pivotPointObject.position.clone();

            // this is original position
            var toPosition = knife[componentName].pivotPointLocation;

            var explodeTween = new TWEEN.Tween(knife[componentName].pivotPointObject.position)
                .to(toPosition, duration)
                .start();
        }
    }

    var exploded = false;

    var guiParams = {
        animate: function() {
            if (!exploded) {
                explode();
                exploded = true;
                animationGuiButton.name('implode!');
            } else {
                implode();
                exploded = false;
                animationGuiButton.name('explode!');
            }
        },
    };

    function explode() {
        tweenToSplayedPositions(200);


        setTimeout(function() {
            tweenToOriginalPositions(240);
            explodeXAxis(450);
        }, 450);
    };
    function implode() {
        implodeXAxis(350);

        setTimeout(function() {
            tweenToOriginalPositions(200);
        }, 150);
    };



    var gui = new dat.GUI();
    var animationGuiButton = gui.add(guiParams, 'animate').name('explode!');



      // show user value from 0 to 1, map it from 0 to -175 deg and convert into radians
      function addKnifeComponentGuiParam(name) {
        var controller = gui.add(
            guiParams,
            name,
            0.0,
            1.0
        );

        controller.name(knife[name].friendlyGuiName);


        controller.onFinishChange(function(value) {

          // scale rotation to fully open rotation range of this component
          var rotationToY = (value * Math.PI) * knife[name].rotationRadians.fullyOpen;
          var toEuler = new THREE.Euler(0, rotationToY, 0, 'XYZ');

          var fromEuler = knife[name].pivotPointObject.rotation.clone();

          var tween = new TWEEN.Tween(fromEuler)
            .to(toEuler, 150)
            .onUpdate(function() {
              knife[name].pivotPointObject.rotation.y = fromEuler.y;
            })
            .start();


        });
      }

    // populate dat gui with movable knife components
    for (var componentName in knife) {
        guiParams[componentName] = 0.0;
        addKnifeComponentGuiParam(componentName);
    }


    render();

	var r = 0;
    function render()
    {
        var delta = clock.getDelta();
        orbitControls.update(delta);

        TWEEN.update();
        // updateDatGui();

        var time = new Date().getTime() * 0.001 * 0.5;

		renderer.render(scene, camera);
        requestAnimationFrame(render);

    }
};
