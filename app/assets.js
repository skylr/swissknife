function loadXHR(url, name)
{
    return new Promise(function(resolve, reject)
    {
        var req = new XMLHttpRequest();
        req.open('GET', url);

        req.onload = function()
        {
            if (req.status == 200)
            {
                resolve({ 'name' : name, 'value' : req.response});
            }
            else
            {
                reject(Error(req.statusText));
            }
        };

        req.onerror = function()
        {
            reject(Error("Network Error"));
        };

        req.send();
    });
}

function loadImage(url, name)
{
    return new Promise(function(resolve, reject)
    {
        var image = new Image();
        image.onload = function()
        {
            resolve({ 'name' : name, 'value' : image});
        }
        image.onerror = function()
        {
            reject();
        }
        image.src = url;
    });
}

function loadCubeMap(loc, name, ext)
{
    return new Promise(function(resolve, reject)
    {
        var items =
        [
            loadImage(loc + 'posx' + ext, 'posx'),
            loadImage(loc + 'negx' + ext, 'negx'),
            loadImage(loc + 'posy' + ext, 'posy'),
            loadImage(loc + 'negy' + ext, 'negy'),
            loadImage(loc + 'posz' + ext, 'posz'),
            loadImage(loc + 'negz' + ext, 'negz')
        ];
        Promise.all(items).then(function(values)
        {
            resolve({ 'name' : name, 'value' : values});
        });
    });
}

function loadOBJ(loc, obj, name, mtl, asset_name, type)
{
    return new Promise(function(resolve, reject)
    {
    	
        if (typeof mtl !== 'undefined' && mtl !== null)
        {
            var mtlloader = new THREE.MTLLoader();
            mtlloader.setBaseUrl('http://localhost:8000/');
            mtlloader.setPath(loc);
            mtlloader.load(mtl, function (materials)
            {
                materials.preload();

                var objloader = new THREE.OBJLoader();
                objloader.setMaterials(materials);
                objloader.setPath(loc);
                objloader.load(obj, function (mesh)
                {
                    resolve({'name' : name, 'value' : mesh, 'asset_name' : asset_name, 'type' : type});
                });
            });
        }
        else
        {
            var objloader = new THREE.OBJLoader();
            objloader.setPath(loc);
            objloader.load(obj, function (mesh)
            {
                resolve({'name' : name, 'value' : mesh, 'asset_name' : asset_name, 'type' : type});
            });
        }
    });
}

function loadModels()
{
    return new Promise(function (resolve, reject)
    {
        var loader = new THREE.OBJLoader();

        var items =
        [
            loadOBJ('/models/', 'bottle_opener.obj', 'bottle_opener', null, 'bottle_opener', 'blade'),
            loadOBJ('/models/', 'mainblade.obj', 'main_blade', null, 'main_blade', 'blade'),
            loadOBJ('/models/', 'nailpointer.obj', 'nail_pointer', null, 'nail_pointer', 'blade'),
            loadOBJ('/models/', 'pick.obj', 'pick', null, 'pick', 'blade'),
            loadOBJ('/models/', 'point_opener.obj', 'point_opener', null, 'point_opener', 'blade'),
            loadOBJ('/models/', 'scissors.obj', 'scissors', null, 'scissors', 'blade'),
            loadOBJ('/models/', 'screwdriver.obj', 'screw_driver', null, 'screw_driver', 'blade'),
            loadOBJ('/models/', 'smaller_blade.obj', 'smaller_blade', null, 'smaller_blade', 'blade'),
            loadOBJ('/models/', 'spacer.obj', 'spacer', null, 'spacer', 'spacer'),
            loadOBJ('/models/', 'spacer_0.obj', 'spacer_0', null, 'spacer_0', 'spacer'),
            loadOBJ('/models/', 'spacer_1.obj', 'spacer_1', null, 'spacer_1', 'spacer'),
            loadOBJ('/models/', 'spacer_2.obj', 'spacer_2', null, 'spacer_2', 'spacer'),
            loadOBJ('/models/', 'thin_spacer.obj', 'thin_spacer', null, 'thin_spacer', 'thin_spacer'),
            loadOBJ('/models/', 'thin_spacer_2.obj', 'thin_spacer_2', null, 'thin_spacer_2', 'thin_spacer_2'),
            loadOBJ('/models/', 'thin_spacer_3.obj', 'thin_spacer_3', null, 'thin_spacer_3', 'thin_spacer_3'),
            loadOBJ('/models/', 'thin_spacer_4.obj', 'thin_spacer_4', null, 'thin_spacer_4', 'thin_spacer_4'),
            loadOBJ('/models/', 'top.obj', 'top', null, 'top', 'handel'),
            loadOBJ('/models/', 'bottom.obj', 'bottom', null, 'bottom', 'handel'),
            loadOBJ('/models/', 'tweezers.obj', 'tweezers', null, 'tweezers', 'handel')
            
            
        ];
        Promise.all(items).then(function(values)
        {
            var obj = {};
            values.forEach(function(elem, i)
            {
                obj[elem.name] = elem.value;
                obj[elem.name].itemType = elem.type;
				obj[elem.name].asset_name = elem.asset_name;
                
            });
            resolve({ 'name' : 'models', 'value' : obj });
        });
    });
}

function loadTextures()
{
    return new Promise(function (resolve, reject)
    {
        var items =
        [
            loadImage('/textures/color_map.png', 'color_map'),
            loadImage('/textures/metal_bolts_None_color.jpg', 'metal_color_map'),
            loadImage('/textures/studio008.png', 'envMap')
            
        
        ];
        Promise.all(items).then(function(values)
        {
            var obj = {};
            values.forEach(function(elem, i)
            {
                obj[elem.name] = elem.value;
            });
            resolve({ 'name' : 'textures', 'value' : obj });
        });
    });
}

function loadShaders()
{
    return new Promise(function (resolve, reject)
    {
        var items =
        [
        

        ];
        Promise.all(items).then(function(values)
        {
            var obj = {};
            values.forEach(function(elem, i)
            {
                obj[elem.name] = elem.value;
            });
            resolve({ 'name' : 'shaders', 'value' : obj });
        });
    });
}

function loadCubeMaps()
{
    return new Promise(function (resolve, reject)
    {
        var items =
        [
            //loadCubeMap('/maps/lightskybox/', 'cubemap', '.png'),
            //loadCubeMap('/maps/refskybox/', 'cubemap', '.png'),
        ];
        Promise.all(items).then(function(values)
        {
            var obj = {};
            values.forEach(function(elem, i)
            {
                obj[elem.name] = elem.value;
            });
            resolve({ 'name' : 'cubemaps', 'value' : obj });
        });
    });
}
