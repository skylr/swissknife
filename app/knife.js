// rotation params are within the range 0 to 1 rad (0deg to 180deg)
var knife = {
    main_blade: {
        pivotPointLocation: new THREE.Vector3(0, 0, -7.5),
        pivotPointObject: null,
        componentOffset: new THREE.Vector3(0, 0, 7),
        rotationRadians: {
            closed: 0.0,
            splayed: -0.77,
            fullyOpen: -0.97
        },
        friendlyGuiName: 'main blade'
    },
    bottle_opener: {
        pivotPointLocation: new THREE.Vector3(0, 0, 7.8),
        pivotPointObject: null,
        componentOffset: new THREE.Vector3(0, 0, -7),
        rotationRadians: {
            closed: 0.0,
            splayed: 0.93,
            fullyOpen: 0.93
        },
        friendlyGuiName: 'bottle opener'
    },
    nail_pointer: {
        pivotPointLocation: new THREE.Vector3(0, 0, 0),
        pivotPointObject: null,
        componentOffset: new THREE.Vector3(0, 0, 0),
        rotationRadians: {
            closed: 0.0,
            splayed: 0.53,
            fullyOpen: 0.53
        },
        friendlyGuiName: 'nail pointer'
    },
    point_opener: {
        pivotPointLocation: new THREE.Vector3(0, 0, -7.5),
        pivotPointObject: null,
        componentOffset: new THREE.Vector3(0, 0, 7.8),
        rotationRadians: {
            closed: 0.0,
            splayed: -0.75,
            fullyOpen: -0.95
        },
        friendlyGuiName: 'point opener'
    },
    scissors: {
        pivotPointLocation: new THREE.Vector3(0, 0, -7.5),
        pivotPointObject: null,
        componentOffset: new THREE.Vector3(0, 0, 7),
        rotationRadians: {
            closed: 0.0,
            splayed: -0.97,
            fullyOpen: -0.97
        },
        friendlyGuiName: 'scissors'
    },
    screw_driver: {
        pivotPointLocation: new THREE.Vector3(1.5, 0, 0),
        pivotPointObject: null,
        componentOffset: new THREE.Vector3(-1.5, 0, 0),
        rotationRadians: {
            closed: 0.0,
            splayed: 0.2,
            fullyOpen: 0.5
        },
        friendlyGuiName: 'screwdriver'
    },
    smaller_blade: {
        pivotPointLocation: new THREE.Vector3(0.2, 0, 7.8),
        pivotPointObject: null,
        componentOffset: new THREE.Vector3(2, 0, -7),
        rotationRadians: {
            closed: 0.0,
            splayed: 0.82,
            fullyOpen: 1.0
        },
        friendlyGuiName: 'smaller blade'
    }
}
